---
layout: page
permalink: /about/index.html
title: Rajath?
tags: [about, rajath, ramakrishna]
modified: 2015-01-07

---

I’m a programmer currently living in Seattle, Washington. I came to the United States in 2012 to do my Masters in Computer Science. I graduated in May 2014 from University of Illinois at Chicago.

I was born and brought up in Bangalore, the Silicon Valley of India. I write code during my free time and contribute to Open Source. I love exploring new languages/frameworks. I’m a *NIX fanatic and I’ve played around with several different flavors of Linux, but recently shifted to OSX. Git, Vim, tmux, Sublime, Android Studio, IntelliJ and my guitar are some of the things I cannot live without.

I like music; I listen to a variety of genre ranging from Trance & Progressive to Classic Rock to Heavy Metal. Though I spend most part of the day in front of my laptop, I do enjoy traveling, hiking and coffee =).
